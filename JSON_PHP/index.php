<!--Vous devrez charger le fichier cs_figures.csv puis afficher chacune des lignes sous forme de card incluses dans semantic ui (crédit Laurent Dev.)
-->

<?php
    $fp = fopen("ex.csv", "r");
    //read csv headers
    $key = fgetcsv($fp,"0",",");
    
    // parse csv rows into array
    $json = array();
        while ($row = fgetcsv($fp,"0",",")) {
        $json[] = array_combine($key, $row);
    }
    
    // release file handle
    fclose($fp);
    
    // encode array to json
    echo "<pre>";
	echo json_encode($json);
	echo "</pre>";

//echo json_encode($json);

?>
