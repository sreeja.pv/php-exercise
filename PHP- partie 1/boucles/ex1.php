Créer une variable et l'initialiser à 0. Tant que cette variable n'atteint pas 10 :

    l'afficher
    incrémenter de 1

<?php
$var = 0;
while($var<=10){
  echo "The number is ". $var . "\n";
  $var++;
}
?>