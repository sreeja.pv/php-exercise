Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre compris en 1 et 100. Tant que la première variable n'est pas supérieur à 20 :

    multiplier la première variable avec la deuxième
    afficher le résultat
    incrémenter la première variable

<?php
$x = 0;
$y=rand(1, 100);
while($x<20){
  $result = $x * $y;
  echo $result ."\n";
  $x++;
}
?>
