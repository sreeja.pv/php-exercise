 Faire une fonction qui prend deux paramètres : age et genre. Le paramètre genre peut prendre comme valeur Homme ou Femme. La fonction doit renvoyer en fonction des paramètres (gérer tous les cas) :

    Vous êtes un homme et vous êtes majeur
    Vous êtes un homme et vous êtes mineur
    Vous êtes une femme et vous êtes majeur
    Vous êtes une femme et vous êtes mineur

<?php
$a = readline("Enter the age ");
$b = readline("Enter the gender ");
function gender($a,$b){
  if($a >= 18){
    if($b === 'M'){
      echo " Vous êtes un homme et vous êtes majeur";
    }elseif($b === 'F'){
      echo " Vous êtes une femme et vous êtes majeur";
    }
  }
  if($a < 18){
    if($b === 'M'){
      echo " Vous êtes un homme et vous êtes mineur";
    }elseif($b === 'F'){
      echo " Vous êtes une femme et vous êtes mineur";
    }
  }
}
gender($a,$b);