<!--Avec le formulaire de l'exercice 5, Si des données sont passées en POST ou en GET, le formulaire ne doit pas être affiché. Par contre les données transmises doivent l'être. Dans le cas contraire, c'est l'inverse. N'utiliser qu'une seule page.
-->

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php

	$var = '<form action="" method="post">
			civilité:<select name="civilité">  
			  <option value="Mr">Mr</option>
			  <option value="Mme">Mme</option> 
			 </select><br>
			Nom: <input type="text" name="nom"><br>
			Prénom: <input type="text" name="prenom"><br>
			<input type="submit" value="Submit"><br>
	</form>';

	if (isset($_POST['prenom']).isset($_POST['nom']).isset($_POST['civilité'])){
		echo "Bonjour ".$_POST['civilité']." ".$_POST['prenom']." ".$_POST['nom'];
	}else {
	    echo $var;
	}
	?>
</body>
</html>
